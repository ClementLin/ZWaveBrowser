package com.clement.zwavebrowser;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import com.clement.zwavebrowser.data.room.AppDatabase;
import com.clement.zwavebrowser.data.room.cameraStatus.CameraStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CameraStatusDaoTest {

    private AppDatabase mDatabase;

    private final String CAM_ID = "001001255";
    private CameraStatus CAMERASTATUS = new CameraStatus(CAM_ID);

    @Before
    public void initDb(){
        mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AppDatabase.class)
                .allowMainThreadQueries()
                .build();
    }

    @After
    public void closeDb(){
        mDatabase.close();
    }

    @Test
    public void getCamerasWhenNoUserInserted(){
        mDatabase.cameraDao()
                .getAll()
                .test()
                .assertNoValues();
    }


    @Test
    public void insertAndGetUser(){
        mDatabase.cameraDao().insert(CAMERASTATUS);

        mDatabase.cameraDao()
                .getCameraById(CAM_ID)
                .test()
                .assertValue(cameraStatus -> cameraStatus.getCam_id().equals(CAM_ID));
    }

    @Test
    public void deleteAndGetUser(){
        mDatabase.cameraDao().insert(CAMERASTATUS);

        mDatabase.cameraDao().deleteAllUsers();

        mDatabase.cameraDao().getAll().test().assertNoValues();
    }
}
