package com.clement.zwavebrowser;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.clement.zwavebrowser.databinding.ActivityMainBinding;
import com.clement.zwavebrowser.data.cameraData.CameraData;
import com.clement.zwavebrowser.viewModel.MainActivityViewModel;
import com.clement.zwavebrowser.viewModel.ViewModelFactory;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ViewModelFactory mViewModelFactory;
    private MainActivityViewModel viewModel;
    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ViewModel
        viewModel = obtainViewModel(this);

        // data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewModel(viewModel);

        initClickEvent();
    }

    public static MainActivityViewModel obtainViewModel(FragmentActivity activity){
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        MainActivityViewModel viewModel =
                ViewModelProviders.of(activity, factory).get(MainActivityViewModel.class);

        return viewModel;
    }

    private void initClickEvent() {
        binding.button.setOnClickListener(view -> {
            Random random = new Random();
            viewModel.addNewCamera(random.nextInt());
        });

        binding.requestBtn.setOnClickListener(view -> {
            String camId = binding.camIdInput.getText().toString();
            String account = binding.adminInput.getText().toString();
            String pw = binding.pwInput.getText().toString();
            CameraData cameraData = new CameraData(camId, account, pw);
            viewModel.doLocateRequest(cameraData);
        });
    }


    @Override
    protected void onStart() {

        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
