package com.clement.zwavebrowser.data.repository;

import com.clement.zwavebrowser.data.room.cameraStatus.CameraStatus;
import com.clement.zwavebrowser.data.room.cameraStatus.LoadCameraStatus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class CameraRepository {

    private static LoadCameraStatus mLoadCameraStatus;

    private static volatile CameraRepository INSTANCE;

    public static CameraRepository getInstance(LoadCameraStatus mLoadCameraStatus){
        if (INSTANCE == null) {
            synchronized (CameraRepository.class) {
                if (INSTANCE == null) {
                    INSTANCE = new CameraRepository(mLoadCameraStatus);
                }
            }
        }
        return INSTANCE;
    }

    private CameraRepository(LoadCameraStatus mLoadCameraStatus) {
        this.mLoadCameraStatus = mLoadCameraStatus;
    }


    public Flowable<List<CameraStatus>> getCameras(){
        return mLoadCameraStatus.getAllCamera();
    }

    public Observable insertCameraId(final String camId){
        return Observable.create(emitter -> {
            mLoadCameraStatus.insertOrUpdateUser(new CameraStatus(camId));
            emitter.onNext(camId);
            emitter.onComplete();
        }).debounce(500, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io());
    }

}
