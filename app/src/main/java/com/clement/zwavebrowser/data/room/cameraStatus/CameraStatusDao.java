package com.clement.zwavebrowser.data.room.cameraStatus;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface CameraStatusDao {

    @Query("SELECT * FROM CameraStatus")
    Flowable<List<CameraStatus>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CameraStatus cameraStatus);

    @Query("SELECT * FROM CameraStatus WHERE cam_id = :camId")
    Maybe<CameraStatus> getCameraById(String camId);


    @Query("DELETE FROM CameraStatus")
    void deleteAllUsers();
}
