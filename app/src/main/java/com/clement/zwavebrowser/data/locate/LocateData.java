package com.clement.zwavebrowser.data.locate;

import com.google.gson.annotations.SerializedName;

/**
 * Created by starvedia on 2017/11/10.
 */

public class LocateData {

    @SerializedName("p2pd")
    public String locateIp;

    public LocateData(String locateIp){
        this.locateIp = locateIp;
    }

}
