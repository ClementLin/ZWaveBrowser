package com.clement.zwavebrowser.data.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.clement.zwavebrowser.data.room.cameraStatus.CameraStatus;
import com.clement.zwavebrowser.data.room.cameraStatus.CameraStatusDao;

@Database(entities = {CameraStatus.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract CameraStatusDao cameraDao();

    private static volatile AppDatabase INSTANCE;
    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "zwave.db")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
