package com.clement.zwavebrowser.data.cameraData;

import com.google.gson.annotations.SerializedName;

/**
 * Created by starvedia on 2017/11/10.
 */

public class CameraData {

    @SerializedName("cam_id")
    public String camId;

    @SerializedName("account")
    public String adminAccount;

    @SerializedName("password")
    public String adminPassword;

    public CameraData(String camId, String adminAccount, String adminPassword){
        this.camId = camId;
        this.adminAccount = adminAccount;
        this.adminPassword = adminPassword;
    }
}
