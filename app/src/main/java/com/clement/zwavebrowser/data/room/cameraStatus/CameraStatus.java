package com.clement.zwavebrowser.data.room.cameraStatus;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class CameraStatus {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "cam_id")
    private String cam_id;

    public CameraStatus(String cam_id) {
        this.cam_id = cam_id;
    }

    public String getCam_id() {
        return cam_id;
    }

    public void setCam_id(String cam_id) {
        this.cam_id = cam_id;
    }
}
