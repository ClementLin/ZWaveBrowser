package com.clement.zwavebrowser.data.room.cameraStatus;

import com.clement.zwavebrowser.CameraDataSource;

import java.util.List;

import io.reactivex.Flowable;

public class LoadCameraStatus implements CameraDataSource{

    private CameraStatusDao mCamDao;

    private static LoadCameraStatus INSTANCE;

    private LoadCameraStatus(CameraStatusDao mCamDao) {
        this.mCamDao = mCamDao;
    }

    public static LoadCameraStatus getInstance(CameraStatusDao tasksDao) {
        if (INSTANCE == null) {
            synchronized (LoadCameraStatus.class) {
                if (INSTANCE == null) {
                    INSTANCE = new LoadCameraStatus(tasksDao);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public Flowable<List<CameraStatus>> getAllCamera() {
        return mCamDao.getAll();
    }

    @Override
    public void insertOrUpdateUser(CameraStatus cameraStatus) {
        mCamDao.insert(cameraStatus);
    }

    @Override
    public void deleteAllUsers() {
        mCamDao.deleteAllUsers();
    }
}
