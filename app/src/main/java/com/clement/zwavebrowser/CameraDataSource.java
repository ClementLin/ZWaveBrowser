package com.clement.zwavebrowser;

import com.clement.zwavebrowser.data.room.cameraStatus.CameraStatus;

import java.util.List;

import io.reactivex.Flowable;

public interface CameraDataSource {
    Flowable<List<CameraStatus>> getAllCamera();

    void insertOrUpdateUser(CameraStatus user);

    void deleteAllUsers();

}
