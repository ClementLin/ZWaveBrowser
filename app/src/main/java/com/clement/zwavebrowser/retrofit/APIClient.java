package com.clement.zwavebrowser.retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by starvedia on 2017/11/10.
 */

public class APIClient {

    private static final String API_URL = "https://www.camreahome.com/";
    private Retrofit retrofit = null;
    private static SVApi client;

    private APIClient(){
        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        client = retrofit.create(SVApi.class);
    }


    public static SVApi getInstance(){
        if (client == null){
            new APIClient();
        }
        return client;
    }

}
