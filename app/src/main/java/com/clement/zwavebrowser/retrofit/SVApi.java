package com.clement.zwavebrowser.retrofit;

import com.clement.zwavebrowser.data.cameraData.CameraData;
import com.clement.zwavebrowser.data.locate.LocateData;
import com.clement.zwavebrowser.data.room.device.DeviceInfo;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by starvedia on 2017/11/10.
 */

public interface SVApi {

    @POST("/api/gssc/locate")
    Observable<LocateData> locate(@Body CameraData cd);

    @POST("/api/gssc/devices")
    Observable<DeviceInfo> getDevices(@Body CameraData cd);
}
