package com.clement.zwavebrowser;

import android.content.Context;
import android.support.annotation.NonNull;

import com.clement.zwavebrowser.data.repository.CameraRepository;
import com.clement.zwavebrowser.data.room.AppDatabase;
import com.clement.zwavebrowser.data.room.cameraStatus.LoadCameraStatus;

public class Injection {

    public static CameraRepository provideCamerasRepository(@NonNull Context context){
        AppDatabase database = AppDatabase.getInstance(context);
        return CameraRepository.getInstance(LoadCameraStatus.getInstance(database.cameraDao()));
    }
}
