package com.clement.zwavebrowser.viewModel;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.clement.zwavebrowser.Injection;
import com.clement.zwavebrowser.data.repository.CameraRepository;

public class ViewModelFactory implements ViewModelProvider.Factory {
    private final CameraRepository mDataSource;
    private static volatile ViewModelFactory INSTANCE;

    private final Application mApplication;

    public static ViewModelFactory getInstance(Application application) {

        if (INSTANCE == null) {
            synchronized (ViewModelFactory.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ViewModelFactory(application,
                            Injection.provideCamerasRepository(application.getApplicationContext()));
                }
            }
        }
        return INSTANCE;
    }

    public ViewModelFactory(Application application, CameraRepository dataSource) {
        this.mApplication = application;
        this.mDataSource = dataSource;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainActivityViewModel.class)) {
            return (T) new MainActivityViewModel(mApplication, mDataSource);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
