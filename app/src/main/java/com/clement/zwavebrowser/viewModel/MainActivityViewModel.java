package com.clement.zwavebrowser.viewModel;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.databinding.ObservableField;
import android.util.Log;

import com.clement.zwavebrowser.data.repository.CameraRepository;
import com.clement.zwavebrowser.retrofit.APIClient;
import com.clement.zwavebrowser.data.cameraData.CameraData;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class MainActivityViewModel extends ViewModel {

    private Context mContext;
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private CameraRepository mCameraRepo;
    private ObservableField<String> cameraCount = new ObservableField<>();
    private ObservableField<String> lastAddCameraId = new ObservableField<>();
    private ObservableField<String> requestStatus = new ObservableField<>("Process status...");

    public ObservableField<String> getCameraCount() {
        return cameraCount;
    }

    public ObservableField<String> getLastAddCameraId() {
        return lastAddCameraId;
    }

    public ObservableField<String> getRequestStatus() {
        return requestStatus;
    }

    public MainActivityViewModel(Application context, CameraRepository cameraDataSource) {
        this.mContext = context;
        this.mCameraRepo = cameraDataSource;
        init();
    }

    private void init() {
        // observe total camera count
        mDisposable.add(mCameraRepo.getCameras()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cameraStatuses -> {
            cameraCount.set(""+cameraStatuses.size());
        }));
    }


    @Override
    protected void onCleared() {
        mDisposable.clear();
        super.onCleared();
    }


    public void addNewCamera(int camId) {
        mDisposable.add(mCameraRepo.insertCameraId(String.valueOf(camId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        id ->{
                            lastAddCameraId.set((String) id);
                        },
                        throwable -> {

                        },
                        () ->{
                            Log.i("ClementDebug", "addNewCamera: onComplete");
                        }));
    }

    public void doLocateRequest(CameraData cameraData){
        requestStatus.set("Sending request");
        APIClient.getInstance().locate(cameraData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        locateData -> {
                            requestStatus.set("Locate success, " + locateData.locateIp);},
                        throwable -> Log.i("ClementDebug", "onError: "),
                        () -> Log.i("ClementDebug", "getLocateIp: onComplete")
                );
    }
}
